import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import task250.Order;

public class OrderMain {
    public static void main(String[] args) throws Exception {
        Order order1 = new Order();
        Order order2 = new Order(2, "Nguyen Van A", 10000);
        Order order3 = new Order(3, "Nguyen Van B", 20000, new Date());
        Order order4 = new Order(4, "Nguyen Van D", 2500, new Date(), true, new String[] { "a", "b", "c" });

        ArrayList<Order> orderList = new ArrayList<>();
        orderList.add(order1);
        orderList.add(order2);
        orderList.add(order3);
        orderList.add(order4);
        for (Order order : orderList) {
            System.out.println(order.toString());
        }

        // System.out.println(orderList.toString());

    }
}

import java.util.ArrayList;

import model.Person;

public class App {
    public static void main(String[] args) throws Exception {
        String s[] = { "cho", "meo" };
        Person person1 = new Person("Nguyen Van A", 25, 65);
        Person person2 = new Person("Nguyen Van B");
        Person person3 = new Person("Nguyen Van C", 30, 70, 1000, s);
        ArrayList<Person> persons = new ArrayList<>();
        persons.add(person1);
        persons.add(person2);
        persons.add(person3);
        System.out.println(persons.toString());

    }
}

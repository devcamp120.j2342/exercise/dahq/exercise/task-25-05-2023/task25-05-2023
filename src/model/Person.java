package model;

public class Person {
    private String name;
    private int age;
    private double weight;
    private long salary;
    private String[] pets;

    public Person(String paramName) {
        this.name = paramName;
        this.age = 18;
        this.weight = 70;
        this.salary = 1000;
    }

    public Person(String paramName, int paramAge, double paramWeight) {
        this.name = paramName;
        this.age = paramAge;
        this.weight = paramWeight;
    }

    public Person() {
    }

    public Person(String name, int age, double weight, long salary, String[] pet) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.salary = salary;
        this.pets = pet;
    }

    @Override
    public String toString() {
        return "name: " + name + " age: " + age + " weight: " + weight + " salary: " + salary + " pets: " + pets;
    }

}

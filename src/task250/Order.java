package task250;

import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

public class Order {
    int id;
    String customerName;
    long price;
    Date orderDate;
    boolean confirm;
    String[] items;

    public Order() {
        this.orderDate = new Date();
    }

    public Order(int paramId, String paramCustomerName, long paramPrice) {
        this.id = paramId;
        this.customerName = paramCustomerName;
        this.price = paramPrice;
        this.orderDate = new Date();

    }

    public Order(int paramId, String paramCustomerName, long paramPrice, Date paramOrderDate) {
        this.id = paramId;
        this.customerName = paramCustomerName;
        this.price = paramPrice;
        this.orderDate = paramOrderDate;
    }

    public Order(int id, String customerName, long price, Date orderDate, boolean confirm, String[] items) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = orderDate;
        this.confirm = confirm;
        this.items = items;
    }

    @Override
    public String toString() {
        // định dạng tiêu chuẩn việt nam
        Locale.setDefault(new Locale("vi", "VN"));
        // định dạng cho ngày tháng
        String pattern = "dd-MMMM-yyyy HH:mm:ss.SSS";
        DateTimeFormatter defaultTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        // định dạng cho giá tien
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
        // return trả ra chuỗi String
        return "id " + id + " ,CustomerName " + customerName + " ,price " +
                usNumberFormat.format(price) + " ,DateTime "
                + defaultTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())

                + " ,confirm " + confirm + " ,item " + Arrays.toString(items);

        // +
        // defaultTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())

    }

}
